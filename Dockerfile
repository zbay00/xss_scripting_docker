FROM alpine:latest

RUN apk add --update --no-cache py-pip

RUN mkdir -p /app

COPY main.py /app/main.py

RUN chmod +x /app/main.py
