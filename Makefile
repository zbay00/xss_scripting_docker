
IMG_NAME=xss_scripting
IMG_TAG=latest


.PHONY: build
build:
	docker build -t $(IMG_NAME):$(IMG_TAG) .

.PHONY: run_debug
run_debug:
	docker run -it \
		-p 8888:8888 \
		--rm $(IMG_NAME):$(IMG_TAG) ./app/main.py

.PHONY: run
run:
	docker run -p 8888:8888 $(IMG_NAME):$(IMG_TAG) ./app/main.py
